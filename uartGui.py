import queue
import threading
import time
import PySimpleGUI as sg
import serial
import serial.tools.list_ports

# pysimplegui https://pysimplegui.readthedocs.io/en/latest/cookbook/
# multithreading https://pysimplegui.trinket.io/demo-programs#/demo-programs/multi-threaded-work

serial_object = serial.Serial()
gui_queue = queue.Queue()  # queue used to communicate between the gui and thread


def refresh():
    comports.clear()
    for comport in serial.tools.list_ports.comports():
        #print(comport.device)
        #print(comport.description)   
        comports.append(comport.device)
    if len(comports) == 0:
        comports.append(' ') 

def connect(port, baud):
    serial_object.port = port
    serial_object.baudrate = baud
    serial_object.timeout = 0 #non blocking
    try:
        serial_object.open()
    except ValueError:
        print ("Enter Baud and Port")
        return
    except:
        print("Port already open")
      

def disconnect():
    if serial_object.is_open:
        serial_object.close()  

def serialRead(gui_queue, stop):
    while serial_object.is_open:
        if stop(): #lambda expression
            break
        returnValue = serial_object.read()
        if returnValue is not b'':
            gui_queue.put(returnValue)
        
def serialWrite(data):
    if serial_object.is_open:
        serial_object.write(data)
    else:
        print("Serial is not open")
    
sg.ChangeLookAndFeel('Dark')
sg.SetOptions(element_padding=(0, 0))

baudrates = ( '4800','9600','19200','38400','57600','74880','115200')
comports = [' ']
refresh() #sets var comports to current detected comports

colComBaud =    [   
                    [sg.T('Com-Port:', pad=((3, 0), 0)), 
                     sg.OptionMenu(values=comports, size=(15, 1), default_value = comports[0], key='comport'), 
                     sg.Text("Port",justification='left')],
                    [sg.T('Baudrate:', pad=((3, 0), 0)), 
                     sg.OptionMenu(values=baudrates, size=(15, 1), default_value = '9600', key='baud'), 
                     sg.Text("baud",justification='left')]
                ]

layout = [  [sg.Output(size=(50, 20), font=('Helvetica 12'))],
            [sg.InputText(size=(40,1 ), key='sendInput',do_not_clear=True),
             sg.Button('Send', button_color=('white', 'grey'), key='Send', bind_return_key=True)],
            [sg.Button('Connect', key='Connect'), sg.Column(colComBaud), sg.Button('Refresh', button_color=('white', 'firebrick3'), key='Refresh')],
        ]

window = sg.Window("Uart", layout, default_element_size=(12, 1), text_justification='r', auto_size_text=False, auto_size_buttons=False,
                   default_button_element_size=(12, 1))
window.Finalize()

#init thread
stop_threads = False
thread_rx = threading.Thread(target=serialRead, args=(gui_queue, lambda: stop_threads), daemon=True)

while True:
    event, values = window.read(timeout=10)
    if event is None:
        if serial_object.is_open:
            serial_object.close()
        exit(69)
    
# --------------- Read next message coming in from threads ---------------
    if serial_object.is_open:
        try:
            message = gui_queue.get_nowait()    # see if something has been posted to Queue
        except queue.Empty:                     # get_nowait() will get exception when Queue is empty
            message = None                      # nothing in queue so do nothing

    # if message received from queue, then some work was completed
        if message is not None:
            print(message.decode('utf-8'), end = '')
   

    if event is 'Refresh':
        refresh()
        window.Element('comport').Update(values=comports)
    if event is 'Connect':
        if serial_object.is_open == True:
            stop_threads = True
            thread_rx.join()
            disconnect()
            window.Element('Connect').Update('Connect',  button_color=('white', 'green'))
            #print("Serial Port Status: " + str(serial_object.is_open))
        else: 
            connect(values['comport'], values['baud'])
            window.Element('Connect').Update('Disconnect', button_color=('white', 'black'))
            #print("Serial Port Status: " + str(serial_object.is_open))
            stop_threads = False
            thread_rx = threading.Thread(target=serialRead, args=(gui_queue, lambda: stop_threads), daemon=True)
            thread_rx.start()      
    if event is 'Send':
        serialWrite(values['sendInput'].encode('ascii'))


 
     